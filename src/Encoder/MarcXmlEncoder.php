<?php

namespace Drupal\bibcite_marcxml\Encoder;

use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Scriptotek\Marc\Collection;

/**
 * MarcXml format encoder.
 */
class MarcXmlEncoder implements DecoderInterface {

  /**
   * The format that this encoder supports.
   *
   * @var string
   */
  protected static $format = 'marcxml';

  /**
   * {@inheritdoc}
   */
  public function supportsDecoding($format) {
    return $format == static::$format;
  }

  /**
   * {@inheritdoc}
   */
  public function decode($data, $format, array $context = []) {
    $records = Collection::fromString($response);
    foreach ($records as $record) {
      echo 'todo';
    }
    return [];
  }

}
