<?php

namespace Drupal\Tests\bibcite_marcxml\Kernel;

use Drupal\bibcite_marcxml\Encoder\MarcXmlEncoder;
use Drupal\Tests\bibcite_import\Kernel\FormatDecoderTestBase;

/**
 * @coversDefaultClass \Drupal\bibcite_marcxml\Encoder\MarcXmlEncoder
 * @group bibcite
 */
class MarcXmlDecodeTest extends FormatDecoderTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = [
    'system',
    'user',
    'serialization',
    'bibcite',
    'bibcite_entity',
    'bibcite_marcxml',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    $this->installConfig([
      'system',
      'user',
      'serialization',
      'bibcite',
      'bibcite_entity',
      'bibcite_marcxml',
    ]);

    $this->encoder = new MarcXmlEncoder();
    $this->format = 'marcxml';
    $this->resultDir = __DIR__ . '/../../data/decoded';
    $this->inputDir = __DIR__ . '/../../data/encoded';
  }

}
