<?php

namespace Drupal\Tests\bibcite_marcxml\Kernel;

use Drupal\bibcite_marcxml\Encoder\MarcXmlEncoder;
use Drupal\Tests\bibcite_export\Kernel\FormatEncoderTestBase;

/**
 * @coversDefaultClass \Drupal\bibcite_marcxml\Encoder\MarcXmlEncoder
 * @group bibcite
 */
class MarcXmlEncodeTest extends FormatEncoderTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = [
    'system',
    'user',
    'serialization',
    'bibcite',
    'bibcite_entity',
    'bibcite_marcxml',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    $this->installConfig([
      'system',
      'user',
      'serialization',
      'bibcite',
      'bibcite_entity',
      'bibcite_marcxml',
    ]);

    $this->encoder = new MarcXmlEncoder();
    $this->format = 'marcxml';
    $this->encodedExtension = 'xml';
    $this->resultDir = __DIR__ . '/../../data/encoded';
    $this->inputDir = __DIR__ . '/../../data/decoded';
  }

}
